/* live reload snippet */
var path = require('path');

var folderMount = function folderMount(connect, point) {
    return connect.static(path.resolve(point));
};

module.exports = function (grunt) {
    'use strict';

    /**
     * Load external grunt helpers & tasks
     */
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.initConfig({
        less: {
            development: {
                options: {
                    yuicompress: true
                },
                files: {
                    "App/css/style.css": "App/less/style.less"
                }
            }
        },

        connect: {
            all: {
                options: {
                    hostname: '0.0.0.0',
                    port: 5000,
                    middleware: function (connect) {
                        return [
                            require('connect-livereload')(),
                            folderMount(connect, 'App')
                        ];
                    }
                }
            }
        },

        watch: {
            html: {
                files: '**/*.html',
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            less: {
                files: 'App/less/*.less',
                tasks: ['less:development']
            },
            css: {
                files: 'App/css/*.css',
                options: {
                    livereload: true
                }
            },
            js: {
                files: 'App/js/**/*.js',
                options: {
                    livereload: true
                }
            }
        }
    });

    /**
     * Define tasks groups
     */
    grunt.registerTask('dev', [
        'dev-build',
        'dev-load'
    ]);

    grunt.registerTask('dev-build', [
        'less:development'
    ]);

    grunt.registerTask('dev-load', [
        'connect',
        'watch'
    ]);

    // Default
    grunt.registerTask('default', ['dev']);
};