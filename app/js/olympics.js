var start = function start (data) {
    console.log('data', data);

    // My Model Park-Class
    var ParkModel = Backbone.Model.extend({

        defaults: {
            "title": "No title given",
            "address": "No address given",
            "description": "No description given",
            "isDone" : false,
            "isFavorite" : false
        },

        initialize: function initialize (options) {
            this.on('change', function () {
                console.log('it changed!');
            });
        }

    });

    // My collection Park-Class
    var ParkCollection = Backbone.Collection.extend({model: ParkModel});

    // een nieuwe instantie van mijn collection
    var parkCollection = new ParkCollection();

    // resetten met de data van de service!
    // (setten kan in dit geval ook aangezien de collection al leeg was...)
    parkCollection.reset(data.groenzone);


    // the detail view
    var ParkView = Backbone.View.extend({
        events: {
            'click' : 'toggleDone',
            'click .favBtn' : 'toggeFav'
        },
        initialize: function initialize (options) {
            _.bindAll(this, 'render');
            this.model.on('change:isFavorite', this.render);
        },
        render: function render () {
            var isFavorite = (this.model.get('isFavorite')) ? 'fav' : '';
            var result = '<li class="clearfix"><div class="thumbnail" style="background-image: url('+this.model.get('image')+')"></div><div class="description"><div class="name">'+this.model.get('title')+'</div><div class="location">'+this.model.get('address')+'</div></div><div class="favBtn '+isFavorite+'"></div></li>';

            this.$el.html(result);

            return this;
        },
        toggleDone: function toggleDone (e) {
            this.model.set('isDone', !this.model.get('isDone'));
            return false;
        },
        toggeFav: function toggeFav (e) {
            this.model.set('isFavorite', !this.model.get('isFavorite'));
            return false;
        }
    });

    // the list view
    var ParkListView = Backbone.View.extend({
        initialize: function initialize (options) {
            _.bindAll(this, 'render');
            this.collection.on('change:isDone', this.render);
        },
        render: function render () {
            console.log('rendering');
            var result = $('<ul></ul>'),
                _this = this;

            this.$el.html(result);

            this.collection.each(function (m) {
                if (m.get('isDone') === false) {
                    var parkView = new ParkView({model:m});
                    _this.$el.find('ul:first').append(parkView.$el);
                    parkView.render();
                }
            });

            return this;
        }
    });

    var ParkListDoneView = ParkListView.extend({
        render: function render () {

            var result = $('<ul></ul>'),
                _this = this;

            this.$el.html(result);

            this.collection.each(function (m) {
                if (m.get('isDone') === true) {
                    var parkView = new ParkView({model:m});
                    _this.$el.find('ul:first').append(parkView.$el);
                    parkView.render();
                }
            });

            return this;
        }
    });

    var parkListTodoView = new ParkListView({collection: parkCollection});

    var parkListDoneView = new ParkListDoneView({collection: parkCollection});

    $('#todo').append(parkListTodoView.$el);
    parkListTodoView.render();

    $('#done').append(parkListDoneView.$el);
    parkListDoneView.render();

}

$(function(){
    $.ajax({
        type: "GET",
        url: 'data/groenzone.json',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            start(data);
        },
        error: function (msg) {
            console.log('Oh my god something is horribly wrong');
        }
    });
});

